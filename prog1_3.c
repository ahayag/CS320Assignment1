#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "prog1_2.h"

int verifyTokens(char *token1, char *token2, char *token3, STACK *stackPtr) //makes sure that user input to STDIN is valid.
{                                                                           //if valid, it will call the appropriate stack function.
    if(strcmp(token1, "pop") == 0)
    {
        if(token2 != '\0') //function returns if user adds another token after "pop"
        {  
            return -1;
        }
        else  //if user only types "pop", the value off the stack is popped off
        {
            printf("%d\n", Pop(stackPtr));
            return 0;
        }
    }
    else if(strcmp(token1, "push") == 0)
    {
        if(token2 == '\0') //returns if user types "push" but does not include an integer to push to the stack
        {  
            return -1;
        }
        else if(token3 != '\0') //returns if user types too many tokens as pushing only requires two tokens
        {
            return -1;
        }
        else if((atoi(token2) != 0) || (strcmp(token2, "0") == 0))  //if user successfully types "push" and an integer, it is pushed to the stack
        {
            Push(stackPtr, atoi(token2));
            return 0;
        }
        else
        {
            return -1;
        }       
    }
    else
    {
        return -1;
    }
}

int main(int argc, char *argv[])
{
    printf("Assignment #1-3, Aaron Hayag, aaron_hayag@yahoo.com\n"); 
 
    char buff[256];
    char *token1 = " ";
    char *token2 = " ";
    char *token3 = " ";
    const char delim[2] = " ";
    STACK* R = MakeStack(3);
    
    if(argc != 2)
    {
        printf("This program expectes a single command line argument.\n");
        return 0;
    }

    for(int i = 0; i < atoi(argv[1]); i++) {
        printf("> ");
        scanf("\n%[^\n]s", buff);
        
        token1 = strtok(buff, delim);
        token2 = strtok(NULL, delim);
        token3 = strtok(NULL, delim);
    
        verifyTokens(token1, token2, token3, R);
    }
    
    return 0;
}