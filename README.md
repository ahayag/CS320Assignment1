Aaron Hayag
aaron_hayag@yahoo.com

CS320 Assignment 1

prog1_1
    Program 1 lets the user enter their name and then outputs a greeting message using the user's full name such as "Hello Firstname Lastname!"
    
    Compilation: 
    gcc prog1_1.c -o prog1_1
    
    Execution:
    ./prog1_1

prog1_2
    Program 2 is a stack implementation using struct. It has four functions: MakeStack, Push, Pop, and Grow. Makestack creates a stack with an
    initial capacity. Grow doubles the capacity of the stack to hold more values. Push allows the user to add values to the stack. It will also
    grow the stack automatically when the stack is full. Pop prints out the value at the top of the stack. If there is no data in the stack, Pop
    returns a -1.

prog1_3
    For program 3, the user inputs an integer as a command line argument and take the same amount of lines of input from STDIN. The user can type
    "push" plus an integer to push data on to the stack or type "pop" to print a value off the stack to STDOUT. The program ignores whitespaces as well
    as any other input.
    
    Example:
    > push 2
    > random words
    > pop
    > 2
    
    Compilation:
    gcc prog1_3.c prog1_2.c -o prog1_3
    
    Execution
    ./prog1_3 (integer here)
    