#include <stdio.h>

int main()
{
    printf("Assignment #1-1, Aaron Hayag, aaron_hayag@yahoo.com\n");
    
    char name[256];
    
    printf("What is your name?\n");
    
    scanf(" %[^\n]s", name);
    
    printf("Hello %s!\n", name);

    return 0;
}