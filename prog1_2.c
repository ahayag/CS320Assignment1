#include<stdio.h>
#include<stdlib.h>
#include "prog1_2.h"

STACK* MakeStack(int initialCapacity) //create stack with initial capacity chosen by user
{
    STACK* s;
    s = malloc(sizeof(STACK));
    s->data = (int*)malloc(initialCapacity * sizeof(int));
    s->capacity = initialCapacity;
    s->size = 0;
} 

void Grow(STACK *stackPtr) //doubles capacity of stack
{
    stackPtr->capacity = (stackPtr->capacity)*2;
    stackPtr->data = (int*)realloc(stackPtr->data, stackPtr->capacity * sizeof(int));
}

void Push(STACK *stackPtr, int data)
{
    if(stackPtr->size == stackPtr->capacity)  //calls grow if stack is full
    {
        Grow(stackPtr);
        stackPtr->data[stackPtr->size] = data;
        stackPtr->size++;                          
        return;
    }
    stackPtr->data[stackPtr->size] = data;
    stackPtr->size++;
}

int Pop(STACK *stackPtr)
{
    if(stackPtr->size == 0)
    {
      return -1;  
    }
    int temp = stackPtr->size;
    stackPtr->size--;
    return stackPtr->data[stackPtr->size];
}